// The project doesn't use Webpack
// It's for WebStorm support

module.exports = {
  resolve: {
    alias: {
      screens: path.resolve(__dirname, "./src/screens"),
      components: path.resolve(__dirname, "./src/components"),
      assets: path.resolve(__dirname, "./src/assets"),
      styles: path.resolve(__dirname, "./src/styles"),
      constants: path.resolve(__dirname, "./src/constants")
    }
  }
};
