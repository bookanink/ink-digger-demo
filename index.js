import configureStore from "./src/store/configureStore";
import * as translations from "constants/translations";
import {LocaleConfig} from "react-native-calendars";
import momentPl from "moment/src/locale/pl";
import {registerRootComponent} from "expo";
import {Provider} from "react-redux";
import App from "./src/App";
import moment from "moment";
import React from "react";

moment.updateLocale("pl", momentPl);

LocaleConfig.locales["pl"] = {
  monthNames: translations.MONTH_NAMES,
  monthNamesShort: translations.MONTH_NAMES,
  dayNames: translations.DAY_NAMES,
  dayNamesShort: translations.SHORT_DAY_NAMES
};
LocaleConfig.defaultLocale = "pl";

const store = configureStore();

registerRootComponent(() => (
  <Provider store={store}>
    <App />
  </Provider>
));
