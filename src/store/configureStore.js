import {createStore, combineReducers, compose, applyMiddleware} from "redux";
import calendar from "./reducer/calendar";
import thunk from "redux-thunk";

const rootReducer = combineReducers({
  calendar
});

const configureStore = () => {
  return createStore(rootReducer, compose(applyMiddleware(thunk)));
};

export default configureStore;
