import {SET_DAYS_OFF, SET_SESSIONS} from "../action/actionTypes";
import {MOCK_DAYS_OFF, MOCK_SESSIONS} from "constants/mockData";

const initialState = {
  sessions: MOCK_SESSIONS,
  daysOff: MOCK_DAYS_OFF
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_SESSIONS:
      return {
        ...state,
        sessions: action.sessions
      };

    case SET_DAYS_OFF:
      return {
        ...state,
        daysOff: action.daysOff
      };

    default:
      return state;
  }
};

export default reducer;
