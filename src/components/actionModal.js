import {viewMarginBottomWithoutSafeArea} from "styles/spacing";
import {ClickableArea} from "components/clickableArea";
import TimesWhite from "assets/svg/times-white.svg";
import {LinearGradient} from "expo-linear-gradient";
import PropTypes from "prop-types";
import React from "react";
import {
  TouchableWithoutFeedback,
  StyleSheet,
  Modal,
  Text,
  View
} from "react-native";

const ActionModal = ({visible, title, children, contentStyle, onClose}) => {
  return (
    <Modal visible={visible} transparent>
      <TouchableWithoutFeedback onPress={onClose}>
        <View>
          <ClickableArea />
          <View style={styles.modalClosingBack} />
        </View>
      </TouchableWithoutFeedback>

      <View style={styles.modalContainer}>
        <LinearGradient
          start={[1, 1]}
          end={[1, 0]}
          colors={["#070523", "#0C0A29", "#131033"]}
        >
          <View style={styles.topBar}>
            <View style={styles.topBarSideBox} />
            <Text style={styles.topBarTitle}>{title}</Text>
            <View style={styles.topBarSideBox}>
              <TouchableWithoutFeedback onPress={onClose}>
                <TimesWhite />
              </TouchableWithoutFeedback>
            </View>
          </View>

          <View style={[styles.modalContentBody, contentStyle]}>
            {children}
          </View>
        </LinearGradient>
      </View>
    </Modal>
  );
};

ActionModal.propTypes = {
  visible: PropTypes.bool,
  title: PropTypes.string,
  contentStyle: PropTypes.object,
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.arrayOf(PropTypes.node)
  ]),
  onClose: PropTypes.func
};

const styles = StyleSheet.create({
  modalClosingBack: {
    width: "100%",
    height: "100%",
    backgroundColor: "#080825",
    opacity: 0.9
  },

  modalContainer: {
    zIndex: 2,
    position: "absolute",
    width: "100%",
    bottom: 0,
    paddingBottom: 24,
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
    backgroundColor: "#080825"
  },
  topBar: {
    padding: 16,
    paddingVertical: 20,
    flexDirection: "row",
    alignItems: "center"
  },
  topBarTitle: {
    flex: 5,
    fontSize: 25,
    textAlign: "center",
    color: "white"
  },
  topBarSideBox: {
    flex: 1,
    alignItems: "flex-end"
  },

  modalContentBody: {
    paddingHorizontal: 16,
    paddingBottom: 24,
    marginBottom: viewMarginBottomWithoutSafeArea,
    alignItems: "center"
  }
});

export default ActionModal;
