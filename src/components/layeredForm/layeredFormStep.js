import ActionIconButton from "components/layeredForm/actionIconButton";
import {StyleSheet, Animated, Dimensions, View} from "react-native";
import FormStepHeader from "components/layeredForm/formStepHeader";
import ArrowRightBlack from "assets/svg/arrow-right-black.svg";
import ArrowLeftBlack from "assets/svg/arrow-left-black.svg";
import React, {useEffect, useState, useRef} from "react";
import TimesBlack from "assets/svg/times-black.svg";
import {Type, Colors} from "styles";
import PropTypes from "prop-types";

const LayeredFormStep = ({
  layeredFormProps: {
    index,
    ableToGoForward,
    currentStep,
    stepsCount,
    goNextStep,
    goPreviousStep,
    goSpecificStep,
    close
  },
  title,
  children,
  hideDescription,
  ...restProps
}) => {
  const screenHeight = Dimensions.get("screen").height;

  const [isModalVisible, setIsModalVisible] = useState(false);
  const transformAnim = useRef(new Animated.Value(screenHeight)).current;
  const opacityAnim = useRef(new Animated.Value(1)).current;

  const isPastFrame = currentStep > index;
  const isCurrentFrame = currentStep === index;
  const isFutureFrame = currentStep < index;
  const isFirstFrame = index === 0;
  const isLastFrame = index === stepsCount - 1;

  useEffect(() => {
    if (isCurrentFrame) {
      setIsModalVisible(true);

      Animated.parallel([
        Animated.timing(transformAnim, {
          toValue: 0,
          duration: 200,
          useNativeDriver: true
        }),
        Animated.timing(opacityAnim, {
          toValue: 1,
          duration: 200,
          useNativeDriver: true
        })
      ]).start();
    } else {
      Animated.parallel([
        Animated.timing(transformAnim, {
          toValue: screenHeight,
          duration: 200,
          useNativeDriver: true
        }),
        Animated.timing(opacityAnim, {
          toValue: 1,
          duration: 200,
          useNativeDriver: true
        })
      ]).start(() => setIsModalVisible(false));
    }
  }, [isPastFrame, isCurrentFrame, isFutureFrame]);

  const modalAnimatedStyles = {
    opacity: opacityAnim,
    transform: [{translateY: transformAnim}]
  };

  const leftSideButton = !isFirstFrame && (
    <ActionIconButton icon={ArrowLeftBlack} onPress={goPreviousStep} />
  );
  const rightSideButton = ableToGoForward ? (
    !isLastFrame && (
      <ActionIconButton icon={ArrowRightBlack} onPress={goNextStep} />
    )
  ) : (
    <ActionIconButton icon={TimesBlack} onPress={close} />
  );

  return (
    isModalVisible && (
      <Animated.ScrollView
        keyboardShouldPersistTaps="handled"
        contentContainerStyle={{flexGrow: 1}}
        style={[styles.modalWrapper, modalAnimatedStyles]}
        {...restProps}
      >
        <View style={styles.modalPrefix} />
        <View style={styles.modalContainer}>
          <FormStepHeader
            title={title}
            description={!hideDescription && `${index + 1}/${stepsCount}`}
            leftSideElement={leftSideButton}
            rightSideElement={rightSideButton}
          />

          {children &&
            children({
              stepIndex: index,
              currentStep,
              goNextStep,
              goPreviousStep,
              goSpecificStep,
              close
            })}
        </View>
      </Animated.ScrollView>
    )
  );
};

LayeredFormStep.propTypes = {
  title: PropTypes.string,
  hideDescription: PropTypes.bool,
  children: PropTypes.func,
  layeredFormProps: PropTypes.shape({
    index: PropTypes.number,
    ableToGoForward: PropTypes.bool,
    currentStep: PropTypes.number,
    steps: PropTypes.number,
    goNextStep: PropTypes.func,
    goPreviousStep: PropTypes.func,
    goSpecificStep: PropTypes.func,
    close: PropTypes.func
  })
};

const styles = StyleSheet.create({
  modalWrapper: {
    ...Type.wrapper
  },
  modalPrefix: {
    backgroundColor: Colors.white,
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
    marginHorizontal: 15,
    height: 16,
    marginBottom: -8,
    opacity: 0.4
  },
  modalContainer: {
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
    flex: 1,
    backgroundColor: Colors.white
  },
  topBarIcon: {
    padding: 10,
    marginBottom: -10
  }
});

export default LayeredFormStep;
