import {StyleSheet, View} from "react-native";
import PropTypes from "prop-types";
import React from "react";

const LayeredFormContent = ({children}) => (
  <View style={styles.modalContent}>{children}</View>
);

LayeredFormContent.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ])
};

const styles = StyleSheet.create({
  modalContent: {
    flex: 1,
    padding: 16,
    paddingTop: 0
  }
});

export default LayeredFormContent;
