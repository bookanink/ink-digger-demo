import LayeredFormContent from "./layeredFormContent";
import LayeredFormStep from "./layeredFormStep";
import PropTypes from "prop-types";
import React from "react";

class LayeredForm extends React.Component {
  state = {
    currentStep: 0
  };

  close = () => {
    const {onClose} = this.props;
    this.setState({currentStep: 0});
    onClose && onClose();
  };

  goNextStep = () => {
    const {currentStep} = this.state;
    const {onFinish} = this.props;

    if (currentStep < this.getStepsCount() - 1) {
      this.setState({currentStep: currentStep + 1});
    } else {
      this.close();
      onFinish && onFinish();
    }
  };

  goPreviousStep = () => {
    const {currentStep} = this.state;

    if (currentStep > 0) {
      this.setState({currentStep: currentStep - 1});
    } else {
      this.close();
    }
  };

  goSpecificStep = step => {
    const {currentStep} = this.state;

    if (currentStep > 0 && currentStep < this.getStepsCount()) {
      this.setState({currentStep: step});
    }
  };

  getStepsCount = () =>
    React.Children.toArray(this.props.children).filter(Boolean).length;

  render() {
    const {children, ableToGoForward} = this.props;
    const {currentStep} = this.state;
    const stepsCount = this.getStepsCount();

    return React.Children.toArray(this.props.children)
      .filter(Boolean)
      .map((child, index) => {
        return React.cloneElement(child, {
          layeredFormProps: {
            index,
            stepsCount,
            currentStep,
            ableToGoForward,
            goNextStep: this.goNextStep,
            goPreviousStep: this.goPreviousStep,
            goSpecificStep: this.goSpecificStep,
            close: this.close
          }
        });
      });
  }
}

LayeredForm.Step = LayeredFormStep;
LayeredForm.Content = LayeredFormContent;

LayeredForm.propTypes = {
  ableToGoForward: PropTypes.bool,
  onClose: PropTypes.func,
  onFinish: PropTypes.func,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ])
};

export default LayeredForm;
