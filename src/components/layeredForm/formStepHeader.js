import {StyleSheet, Text, View} from "react-native";
import PropTypes from "prop-types";
import React from "react";
import {Type, Spacing, Colors, Typography, Buttons, Onboarding} from "styles";

const FormStepHeader = ({
  title,
  description,
  leftSideElement,
  rightSideElement
}) => {
  return (
    <View style={styles.topBar}>
      <View style={styles.topBarLeftSideBox}>{leftSideElement}</View>

      <View style={styles.topBarCenterBox}>
        {title && <Text style={styles.titleText}>{title}</Text>}
        {description && (
          <Text style={styles.descriptionText}>{description}</Text>
        )}
      </View>

      <View style={styles.topBarRightSideBox}>{rightSideElement}</View>
    </View>
  );
};

FormStepHeader.propTypes = {
  title: PropTypes.string,
  description: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
  leftSideElement: PropTypes.node,
  rightSideElement: PropTypes.node
};

const styles = StyleSheet.create({
  topBar: {
    ...Spacing.container,
    paddingTop: 16,
    paddingBottom: 16,
    flexDirection: "row"
  },
  topBarCenterBox: {
    flex: 5,
    alignItems: "center"
  },
  topBarLeftSideBox: {
    flex: 1,
    alignItems: "flex-start"
  },
  topBarRightSideBox: {
    flex: 1,
    alignItems: "flex-end"
  },

  titleText: {
    color: Colors.navyBlue,
    fontSize: Typography.largeFontSize,
    lineHeight: 44,
    flex: 1,
    textAlign: "center",
    fontFamily: Typography.fontFamilyBrand,
  },
  descriptionText: {
    fontSize: Typography.fontSize,
    fontFamily: Typography.fontFamilyBase
  }
});

export default FormStepHeader;
