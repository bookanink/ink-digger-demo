import {View, StyleSheet, TouchableWithoutFeedback} from "react-native";
import {MONTH_NAMES} from "constants/translations";
import {Colors} from "styles/index";
import Text from "components/text";
import PropTypes from "prop-types";
import React from "react";

const CalendarHeader = ({
  current,
  onPressPrevMonth,
  onPressNextMonth,
  darkMode
}) => {
  const curMonth = current.getMonth();
  const curYear = current.getFullYear();

  const computedPrevMonthStyle = [
    styles.sideMonth,
    darkMode && styles.sideMonthDark
  ];
  const computedCurrentMonthStyle = [
    styles.currentMonth,
    darkMode && styles.currentMonthDark
  ];
  const computedNextMonthStyle = [
    styles.sideMonth,
    darkMode && styles.sideMonthDark,
    styles.nextMonth
  ];

  return (
    <View style={styles.root}>
      <View style={styles.monthsContainer}>
        <TouchableWithoutFeedback onPress={onPressPrevMonth}>
          <Text size="base" dark withOpacity style={computedPrevMonthStyle}>
            {MONTH_NAMES[curMonth - 1]}
          </Text>
        </TouchableWithoutFeedback>
        <Text dark size={16} style={computedCurrentMonthStyle}>
          {MONTH_NAMES[curMonth]} {curYear}
        </Text>
        <TouchableWithoutFeedback onPress={onPressNextMonth}>
          <Text size="base" dark withOpacity style={computedNextMonthStyle}>
            {MONTH_NAMES[curMonth + 1]}
          </Text>
        </TouchableWithoutFeedback>
      </View>
      <View style={styles.divider} />
    </View>
  );
};

CalendarHeader.propTypes = {
  current: PropTypes.object,
  onPressPrevMonth: PropTypes.func,
  onPressNextMonth: PropTypes.func,
  darkMode: PropTypes.bool
};

const styles = StyleSheet.create({
  root: {
    alignItems: "center",
    marginTop: 8,
    marginBottom: 24
  },
  monthsContainer: {
    flexDirection: "row"
  },

  currentMonth: {
    flex: 1,
    textAlign: "center"
  },
  currentMonthDark: {
    color: Colors.white
  },
  sideMonth: {},
  sideMonthDark: {
    color: Colors.white
  },
  nextMonth: {
    textAlign: "right"
  },

  divider: {
    backgroundColor: Colors.gold,
    width: 15,
    height: 2,
    borderRadius: 2,
    marginTop: 8
  }
});

export default CalendarHeader;
