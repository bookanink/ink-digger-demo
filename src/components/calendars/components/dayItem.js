import {View, TouchableOpacity, StyleSheet} from "react-native";
import Star from "assets/svg/star.svg";
import Dot from "assets/svg/dot.svg";
import {Colors} from "styles/index";
import PropTypes from "prop-types";
import Text from "components/text";
import InfoIcon from "./infoIcon";
import React from "react";

const DayItem = ({
  date,
  darkMode,
  isDisabled,
  isPressDisabled,
  isToday,
  isSelected,
  isPeriodStart,
  isPeriodEnd,
  isInPeriod,
  hasDot,
  hasStar,
  hasActiveIcon,
  onPress
}) => {
  const mergedContainerStyles = [
    styles.container,
    isToday && styles.containerToday,
    (isSelected || isPeriodStart || isPeriodEnd) && styles.containerSelected
  ];
  const mergedTextStyles = [
    [styles.text, darkMode && styles.textDark],
    isDisabled &&
      !isSelected && [styles.textDisabled, darkMode && styles.textDisabledDark],
    (isSelected || isPeriodStart || isPeriodEnd) && styles.textSelected,
    {fontSize: 18}
  ];

  const mergedTopBorderLineStyles = [
    (isPeriodStart || isPeriodEnd || isInPeriod) && styles.borderLine,
    styles.borderLineTop,
    isPeriodEnd && styles.borderLineLeftHalf,
    isPeriodStart && styles.borderLineRightHalf
  ];
  const mergedBottomBorderLineStyles = [
    (isPeriodStart || isPeriodEnd || isInPeriod) && styles.borderLine,
    styles.borderLineBottom,
    isPeriodEnd && styles.borderLineLeftHalf,
    isPeriodStart && styles.borderLineRightHalf
  ];

  return (
    <TouchableOpacity
      style={styles.root}
      onPress={() => onPress && onPress(date)}
      disabled={isPressDisabled}
    >
      <View style={{position: "relative", width: "100%", alignItems: "center"}}>
        <View style={mergedTopBorderLineStyles} />

        <View style={mergedContainerStyles}>
          <Text family="baseBold" style={mergedTextStyles}>
            {date.day}
          </Text>
        </View>

        <View style={mergedBottomBorderLineStyles} />
      </View>

      <View style={styles.marksContainer}>
        {(hasStar || hasDot) && (
          <InfoIcon
            icon={hasStar ? Star : Dot}
            active={hasActiveIcon}
            disabled={isDisabled}
            darkMode={darkMode}
          />
        )}
      </View>
    </TouchableOpacity>
  );
};

DayItem.propTypes = {
  date: PropTypes.object,
  darkMode: PropTypes.bool,
  isDisabled: PropTypes.bool,
  isPressDisabled: PropTypes.bool,
  isToday: PropTypes.bool,
  isSelected: PropTypes.bool,
  isPeriodStart: PropTypes.bool,
  isPeriodEnd: PropTypes.bool,
  isInPeriod: PropTypes.bool,
  hasDot: PropTypes.bool,
  hasStar: PropTypes.bool,
  hasActiveIcon: PropTypes.bool,
  onPress: PropTypes.func
};

const styles = StyleSheet.create({
  root: {
    alignItems: "center",
    alignSelf: "stretch",
    marginLeft: -1
  },

  container: {
    width: 26,
    height: 24,
    justifyContent: "center",
    borderWidth: 1,
    borderColor: "transparent",
    borderTopLeftRadius: 3.8,
    borderBottomRightRadius: 3.8
  },
  containerToday: {
    borderColor: "#AC891B"
  },
  containerSelected: {
    borderColor: Colors.goldBorder,
    backgroundColor: Colors.goldBorder
  },

  text: {
    textAlign: "center",
    color: Colors.navyBlue,
    fontSize: 16,
    lineHeight: 19
  },
  textDark: {
    color: Colors.white
  },
  textSelected: {
    color: Colors.white
  },
  textDisabled: {
    opacity: 0.2
  },
  textDisabledDark: {
    opacity: 0.4
  },

  marksContainer: {
    width: 24,
    height: 8,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },

  borderLine: {
    position: "absolute",
    width: "100%",
    height: 1,
    backgroundColor: Colors.gold
  },
  borderLineTop: {
    top: 0
  },
  borderLineBottom: {
    bottom: 0
  },
  borderLineLeftHalf: {
    width: "50%",
    left: 0
  },
  borderLineRightHalf: {
    width: "50%",
    right: 0
  }
});

export default React.memo(DayItem);
