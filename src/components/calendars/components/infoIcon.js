import {Colors} from "styles/index";
import PropTypes from "prop-types";
import React from "react";

const InfoIcon = ({icon: Icon, disabled, active, darkMode}) => {
  const color = getIconColor({disabled, active, darkMode});
  return <Icon height={8} color={color} />;
};

const getIconColor = ({disabled, active, darkMode}) => {
  if (active) {
    const opacity = disabled ? "60" : "";
    return Colors.gold + opacity;
  }

  if (darkMode) {
    const opacity = disabled ? "40" : "60";
    return Colors.white + opacity;
  }

  const opacity = disabled ? "40" : "60";
  return Colors.navyBlue + opacity;
};

InfoIcon.propTypes = {
  icon: PropTypes.func,
  disabled: PropTypes.bool,
  active: PropTypes.bool,
  darkMode: PropTypes.bool
};

export default InfoIcon;
