import CalendarHeader from "./components/calendarHeader";
import React, {useMemo, useState, useRef} from "react";
import HeadlessCalendar from "./headlessCalendar";
import {addMonthsToDate} from "../../utils/date";
import Swiper from "react-native-swiper";
import PropTypes from "prop-types";
import {View} from "react-native";

const SwipeCalendar = ({visibleMonthsCount = 10, darkMode, ...restProps}) => {
  const swiperRef = useRef(null);

  const [currentPage, setCurrentPage] = useState(visibleMonthsCount);
  const pagesCount = visibleMonthsCount * 2 + 1;

  const startDates = useMemo(() => {
    return Array(pagesCount)
      .fill(null)
      .map((_, index) =>
        addMonthsToDate(new Date(), index - visibleMonthsCount)
      );
  }, []);

  const calendars = Array(pagesCount)
    .fill(null)
    .map((_, index) => (
      <HeadlessCalendar
        key={index}
        current={startDates[index]}
        darkMode={darkMode}
        {...restProps}
      />
    ));

  const goPrevPage = () => swiperRef.current && swiperRef.current.scrollBy(-1);
  const goNextPage = () => swiperRef.current && swiperRef.current.scrollBy(1);

  return (
    <View style={{height: 380}}>
      <CalendarHeader
        current={startDates[currentPage]}
        onPressPrevMonth={goPrevPage}
        onPressNextMonth={goNextPage}
        darkMode={darkMode}
      />
      <Swiper
          ref={swiperRef}
          index={visibleMonthsCount}
          onIndexChanged={setCurrentPage}
          loadMinimal
          showsPagination={false}
          loop={false}
      >
        {calendars}
      </Swiper>
    </View>
  );
};

SwipeCalendar.propTypes = {
  ...HeadlessCalendar.propTypes,
  visibleMonthsCount: PropTypes.number
};

export default SwipeCalendar;
