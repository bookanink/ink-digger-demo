import * as Colors from "./colors";
import * as Typography from "./typography";

export const text = {
  fontSize: Typography.baseFontSize,
  fontFamily: Typography.fontFamilyBrand,
  color: Colors.white
};
