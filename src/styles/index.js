import * as Sizes from "./sizes";
import * as Type from "./type";
import * as Buttons from "./buttons";
import * as Colors from "./colors";
import * as Spacing from "./spacing";
import * as Typography from "./typography";

export {Sizes, Type, Typography, Spacing, Colors, Buttons};
