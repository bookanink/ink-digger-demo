import * as Sizes from "./sizes";
import * as Colors from "./colors";

export const fontFamilyBase = "Cabin";
export const fontFamilyBaseBold = "CabinBold";
export const fontFamilyBrand = "LexendDeca";

export const largeFontSize = Sizes.isSmall ? 24 : 18;
export const baseFontSize = Sizes.isSmall ? 16 : 14;
export const smallFontSize = Sizes.isSmall ? 14 : 12;

export const naviText = {
  color: Colors.white,
  fontSize: largeFontSize,
  fontFamily: fontFamilyBrand,
  lineHeight: 24
};
