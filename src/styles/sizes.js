import {Dimensions} from "react-native";

// Retrieve initial screen's width
let screenWidth = Dimensions.get("window").width;

let screenHeight = Dimensions.get("window").height;

export const isSmall = screenHeight / screenWidth <= 1.8;
