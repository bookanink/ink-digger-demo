export const navyBlue = "#070523";

export const white = "#ffffff";
export const gold = "#B08E1C";
export const goldBorder = "#AC891B";
export const goldGrayed = "#B6B5BF";
export const red = "#AC3F1B";

export const bg = navyBlue;
