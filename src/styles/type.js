import * as Spacing from "./spacing";
import * as Colors from "./colors";

export const wrapper = {
  flexGrow: 1,
  backgroundColor: Colors.bg,
  paddingTop: Spacing.viewMarginTop
};

export const container = {
  ...Spacing.container,
  paddingTop: Spacing.containerPaddingnTop,
  flex: 1
};
