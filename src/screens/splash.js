import {StyleSheet, View} from "react-native";
import Logo from "assets/svg/logo.svg";
import {Colors} from "styles";
import React from "react";

const SplashScreen = () => {
  return (
    <View style={styles.container}>
      <Logo width={300} height={300} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: Colors.navyBlue
  }
});

export default SplashScreen;
