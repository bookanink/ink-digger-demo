import {StyleSheet, View} from "react-native";
import React from "react";

const PropertiesRow = props => {
  return <View style={styles.container} {...props} />;
};

const styles = StyleSheet.create({
  container: {
    width: "100%",
    flexDirection: "row"
  }
});

export default PropertiesRow;
