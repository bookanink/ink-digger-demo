import LabeledItemProperty from "components/labeledItemProperty";
import {childrenPropType} from "../../../../utils/propTypes";
import ImageWithDefault from "components/imageWithDefault";
import LayeredForm from "components/layeredForm";
import {StyleSheet, View} from "react-native";
import PropertiesRow from "./propertiesRow";
import {Spacing} from "styles/index";
import PropTypes from "prop-types";
import React from "react";

const EventDetails = ({title, imageUri, footer, onGoBackRequest, children}) => {
  return (
    <View style={{flex: 1}}>
      <LayeredForm onClose={onGoBackRequest}>
        <LayeredForm.Step title={title} hideDescription>
          {() => (
            <>
              <LayeredForm.Content>
                <View style={styles.imageContainer}>
                  <ImageWithDefault
                    style={styles.image}
                    resizeMethod="scale"
                    resizeMode="cover"
                    source={{uri: imageUri}}
                    defaultImage={require("assets/images/avatar_artist.png")}
                  />
                </View>
                <View style={{flex: 1}}>{children}</View>
              </LayeredForm.Content>
              <View style={{flexDirection: "row"}}>{footer}</View>
            </>
          )}
        </LayeredForm.Step>
      </LayeredForm>
    </View>
  );
};

EventDetails.PropsRow = PropertiesRow;
EventDetails.Prop = LabeledItemProperty;

EventDetails.propTypes = {
  title: PropTypes.string,
  imageUri: PropTypes.string,
  onGoBackRequest: PropTypes.func,
  footer: childrenPropType,
  children: childrenPropType
};

const styles = StyleSheet.create({
  imageContainer: {
    alignItems: "center",
    marginBottom: Spacing.viewMarginBottom
  },
  image: {
    height: 136,
    aspectRatio: 1,
    borderRadius: 4
  }
});

export default EventDetails;
