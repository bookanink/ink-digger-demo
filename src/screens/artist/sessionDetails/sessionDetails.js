import {View, StyleSheet, ScrollView} from "react-native";
import Button from "components/buttons/button";
import EventDetails from "./components";
import React, {useState} from "react";
import Text from "components/text";
import moment from "moment";

const SessionDetails = ({navigation, route}) => {
  const {session} = route.params;

  const [areDetailsHidden, setAreDetailsHidden] = useState(true);

  const footerActions = session.proposedChanges ? (
    <Button
      type="primary"
      title="Prośba wysłana"
      disabled
      containerStyle={{flex: 1}}
    />
  ) : (
    <Button
      dark
      title="Akceptuj"
      containerStyle={{flex: 1}}
      onPress={navigation.goBack}
    />
  );

  return (
    <ScrollView contentContainerStyle={{flexGrow: 1}}>
      <EventDetails
        title="Szczegóły sesji"
        onGoBackRequest={navigation.goBack}
        footer={footerActions}
      >
        {session.proposedChanges && (
          <View style={styles.proposedContainer}>
            <EventDetails.Prop
              title="Nowy termin"
              value={formatDateToString(session.proposedChanges.date)}
              fullWidth
              dark
            />
            <View style={styles.waitingStatusContainer}>
              <Text dark>oczekuje</Text>
              <View style={styles.statusDot} />
            </View>
          </View>
        )}

        {session.proposedChanges && areDetailsHidden && (
          <Button
            dark
            title="Pokaż szczegóły sesji"
            onPress={() => setAreDetailsHidden(false)}
          />
        )}

        {(!session.proposedChanges || !areDetailsHidden) && (
          <>
            <EventDetails.PropsRow>
              <EventDetails.Prop title="Klient" value={session.name} dark />
              <EventDetails.Prop
                title="Cena"
                value={session.price + " pln"}
                dark
              />
            </EventDetails.PropsRow>
            <EventDetails.PropsRow>
              <EventDetails.Prop
                title="Studio/spot"
                value={session.place}
                dark
              />
              <EventDetails.Prop
                title="Termin"
                value={formatDateToString(session.date)}
                dark
              />
            </EventDetails.PropsRow>
            <EventDetails.PropsRow>
              <EventDetails.Prop
                title="Czas trwania"
                value={session.length + "h"}
                dark
              />
            </EventDetails.PropsRow>
          </>
        )}
      </EventDetails>
    </ScrollView>
  );
};

const formatDateToString = date =>
  moment(date).format("ddd. D MMMM, YYYY, HH:mm");

const styles = StyleSheet.create({
  proposedContainer: {
    flexDirection: "row",
    alignItems: "flex-end"
  },
  waitingStatusContainer: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 18
  },
  statusDot: {
    width: 8,
    height: 8,
    backgroundColor: "#FFB008",
    borderRadius: 4,
    marginLeft: 8
  }
});

export default SessionDetails;
