import {useNavigation} from "@react-navigation/native";
import {artistScreens} from "constants/screenNames";
import LogoIcon from "assets/svg/logo.svg";
import React from "react";
import {
  TouchableWithoutFeedback,
  StyleSheet,
  FlatList,
  View,
  Text
} from "react-native";

const SessionItem = ({name, date, session}) => {
  const navigation = useNavigation();
  return (
    <View style={styles.itemContainer}>
      <Text style={styles.time}>{date.getHours()}:00</Text>
      <TouchableWithoutFeedback
        onPress={() =>
          navigation.navigate(artistScreens.SESSION_DETAILS, {session})
        }
      >
        <View style={styles.labelContainer}>
          <View style={styles.iconContainer}>
            <LogoIcon width={45} height={45} />
          </View>
          <Text style={styles.name}>{name}</Text>
        </View>
      </TouchableWithoutFeedback>
    </View>
  );
};

const sessionList = ({sessions}) => (
  <View style={styles.container}>
    <FlatList
      data={sessions}
      extractKey={item => item.id}
      renderItem={({item}) => (
        <SessionItem date={item.date} name={item.name} session={item} />
      )}
    />
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  itemContainer: {
    flexDirection: "row",
    flex: 1,
    alignItems: "center",
    marginBottom: 24
  },
  iconContainer: {
    height: 48,
    width: 48,
    justifyContent: "center",
    alignItems: "center",
    borderRightWidth: 1,
    borderColor: "rgba(7,5,35,0.3)"
  },
  time: {
    opacity: 0.8,
    color: "white",
    width: 40,
    height: 28,
    textAlign: "center",
    marginRight: 12,
    fontSize: 12
  },
  labelContainer: {
    flex: 1,
    backgroundColor: "white",
    borderRadius: 4,
    borderTopLeftRadius: 0,
    flexDirection: "row"
  },
  name: {
    flex: 1,
    color: "#383B41",
    fontSize: 14,
    fontFamily: "Cabin",
    padding: 15
  }
});

export default sessionList;
