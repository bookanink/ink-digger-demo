import CalendarDayPicker from "components/calendars/calendarDayPicker";
import {View, StyleSheet, ScrollView} from "react-native";
import Reservations from "assets/svg/reservations.svg";
import IconButton from "components/buttons/iconButton";
import {artistScreens} from "constants/screenNames";
import * as Typography from "styles/typography";
import CheckBox from "components/checkbox";
import SessionsList from "./sessionsList";
import Text from "components/text";
import React from "react";

export const calendarLayout = ({
  selectedDate,
  isDayOffCheckBoxOn,
  onDaySelect,
  daysOff,
  sessions,
  onDayOffCheckBoxChange,
  displayedSessions,
  areUndisplayedReservations
}) => (
  <View style={styles.container}>
    <ScrollView>
      <View style={{flex: 1}}>
        <View style={styles.topBar}>
          <Text family="brand" style={styles.screenTitle}>
            Kalendarz
          </Text>
          <View style={styles.topBarSideContent}>
            <IconButton
              icon={Reservations}
              marked={areUndisplayedReservations}
            />
          </View>
        </View>
        <CalendarDayPicker
          daysOff={daysOff}
          sessions={sessions}
          selectedDay={selectedDate}
          onSelectedDayChange={onDaySelect}
          areDisabledDaysPressable
          darkMode
        />

        <View style={styles.dayOffSwitchContainer}>
          <CheckBox
            value={isDayOffCheckBoxOn}
            onValueChange={onDayOffCheckBoxChange}
          />
          <Text style={{marginLeft: 15}}>
            {isDayOffCheckBoxOn ? "Dziś odpoczywam" : "Ustaw dzień wolny"}
          </Text>
        </View>

        {displayedSessions.length ? (
          <SessionsList sessions={displayedSessions} />
        ) : (
          <Text style={styles.noSessionsMessage}>Brak umówionych sesji</Text>
        )}
      </View>
    </ScrollView>
  </View>
);

const styles = StyleSheet.create({
  container: {
    paddingLeft: 16,
    paddingRight: 16,
    backgroundColor: "#080825",
    height: "100%",
    justifyContent: "space-between"
  },
  topBar: {
    height: 44,
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 24
  },
  screenTitle: {
    flex: 1,
    textAlign: "center",
    fontSize: 18,
    height: 23,
    ...Typography.naviText
  },
  topBarSideContent: {
    width: 30,
    justifyContent: "flex-end",
    alignItems: "center"
  },
  dayOffSwitchContainer: {
    flexDirection: "row",
    marginBottom: 24,
    opacity: 0.9
  },
  noSessionsMessage: {
    textAlign: "center",
    margin: 24,
    opacity: 0.9
  }
});
