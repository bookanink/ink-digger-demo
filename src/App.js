import SessionDetails from "screens/artist/sessionDetails/sessionDetails";
import CalendarScreen from "screens/artist/calendar/calendarScreen";
import {artistScreens, commonScreens} from "constants/screenNames";
import {FORM_TRANSITION_CONFIG} from "./utils/screenTransitions";
import {NavigationContainer} from "@react-navigation/native";
import {createStackNavigator} from "@react-navigation/stack";
import SafeScreenWrapper from "components/safeScreenWrapper";
import {StatusBar} from "expo-status-bar";
import SplashScreen from "screens/splash";
import {useFonts} from "expo-font";
import React from "react";

const customFonts = {
  LexendDeca: require("assets/fonts/LexendDeca-Regular.ttf"),
  Cabin: require("assets/fonts/CabinInk-Regular.ttf"),
  CabinBold: require("assets/fonts/CabinInk-SemiBold.ttf")
};

const RootStack = createStackNavigator();

const App = () => {
  const [fontsLoaded] = useFonts(customFonts);

  return (
    <SafeScreenWrapper>
      <NavigationContainer>
        <StatusBar translucent backgroundColor="transparent" />
        <RootStack.Navigator headerMode={"none"}>
          {!fontsLoaded && (
            <RootStack.Screen
              name={commonScreens.SPLASH}
              component={SplashScreen}
            />
          )}
          {fontsLoaded && (
            <>
              <RootStack.Screen
                name={artistScreens.home.CALENDAR}
                component={CalendarScreen}
              />
              <RootStack.Screen
                name={artistScreens.SESSION_DETAILS}
                component={SessionDetails}
                options={FORM_TRANSITION_CONFIG}
              />
            </>
          )}
        </RootStack.Navigator>
      </NavigationContainer>
    </SafeScreenWrapper>
  );
};

export default App;
