import moment from "moment";

export const formatDateToCalendarFormat = date =>
  moment(date).format("YYYY-MM-DD");
