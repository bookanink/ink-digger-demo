import {Dimensions} from "react-native";

export const getColumnsSize = (columns = 1) => {
  // 5 * 16 is 5, 16px gutter size for 4 column grid
  return columns * (Dimensions.get("window").width / 4) - (4 - columns) * 16;
};

export const getColumnsSizeWithGutters = (columns = 1) => {
  // 5 * 16 is 5, 16px gutter size for 4 column grid
  return columns * (Dimensions.get("window").width / 4) - 5 * 16;
};

export const isScreenSmall = Dimensions.get("window").width < 360;
